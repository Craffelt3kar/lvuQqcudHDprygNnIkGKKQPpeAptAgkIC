---
description: 자동으로 원하는 특정 영웅을 선택합니다.
---

# Auto Lock Hero: 자동 영웅 고정

<figure><img src="../../.gitbook/assets/menu_misc_autolockhero_enabled_en.png" alt=""><figcaption><p>AUTO LOCK HERO</p></figcaption></figure>

영웅 선택 창이 뜨면 자동으로 미리 지정한 영웅을 가능한 한 빨리 픽합니다.

영웅 아이콘을 클릭하여 대상 영웅을 지정할 수 있으며, 현재 선택된 영웅은 아랫쪽의 `CURRENT AUTO LOCK HERO` 아래쪽에 나타납니다.

아랫쪽의 `Reset` 버튼을 클릭하여 현재 지정된 영웅을 초기화할 수 있습니다.

영웅이 지정되지 않으면, 이 기능은 자동으로 비활성화됩니다.
