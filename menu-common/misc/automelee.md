---
description: 자동 근접 공격에 관해 설정할 수 있습니다.
---

# Auto Melee: 자동 근접 공격

<figure><img src="../../.gitbook/assets/menu_misc_automelee_en.png" alt=""><figcaption><p>AUTO MELEE</p></figcaption></figure>

활성화 시, 근접 공격 한 번이면 죽을 정도로 체력이 적은 적을 자동으로 근접 공격을 사용해 처치합니다.

## Enabled

자동 근접 공격 기능을 활성화할지의 여부입니다.

## Screentime

지정된 시간 이상동안 화면에 보여진 적에게만 자동으로 근접 공격을 사용합니다. (단위: 초)

예를 들어 값이 `0.2`인 경우, 0.2초 이상 화면 안에 존재했던 적들만을 자동 근접 공격의 대상으로 삼습니다.
