# Misc: 기타 기능

에임봇 또는 ESP 어느 부류에도 속하지 않는 기능들은 이 분류에 속하게 됩니다.

현재 이 분류에 속한 기능은 자동 근접 공격, 자동 영웅 고정 2 가지가 있으며 해당 기능들에 대한 설명은 각각의 문서를 참고하세요.

{% content-ref url="automelee.md" %}
[Auto Melee: 자동 근접 공격](automelee.md)
{% endcontent-ref %}

{% content-ref url="autolockhero.md" %}
[Auto Lock Hero: 자동 영웅 고정](autolockhero.md)
{% endcontent-ref %}

{% content-ref url="exploits.md" %}
[Exploits: 취약점 악용](exploits.md)
{% endcontent-ref %}
