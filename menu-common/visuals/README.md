# Visuals: 시각 기능

시각 정보 계열(ESP, 레이더 등)에 속하는 기능들은 이 분류에 속하게 됩니다.

현재 이 분류에 속한 하위 분류는 Native ESP, 2D ESP, 3D ESP 세 가지로 나뉘며 각각에 대한 설명은 해당하는 문서를 참고하세요.

{% content-ref url="native.md" %}
[Native: 네이티브 ESP](native.md)
{% endcontent-ref %}

{% content-ref url="2drender.md" %}
[2D Render: 2D ESP](2drender.md)
{% endcontent-ref %}

{% content-ref url="3drender.md" %}
[3D Render: 3D ESP](3drender.md)
{% endcontent-ref %}
