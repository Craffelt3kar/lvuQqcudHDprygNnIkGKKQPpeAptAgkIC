---
description: 이 메뉴에서는 화면 상에 2D로 그려질 수 있는 ESP 요소들에 대해 설정할 수 있습니다.
---

# 2D Render: 2D ESP

<figure><img src="../../.gitbook/assets/menu_visuals_2drender_en.png" alt=""><figcaption><p>2D RENDER ESP</p></figcaption></figure>

## Ultimate Charge

적 영웅들의 궁극기 충전률 게이지를 표시할지의 여부입니다.

{% hint style="information" %}
만약 적 영웅들이 5명보다 많을 경우 (데스매치, 사설방 등의 환경) 궁극기 게이지는 자동으로 숨겨집니다.

이는 시야를 방해하지 않기 위한 조치로, 정상적인 현상입니다.
{% endhint %}

<figure><img src="../../.gitbook/assets/showcase_ultimatecharge.png" alt=""><figcaption><p>사용 예시</p></figcaption></figure>

## Ultimate Charge Position

Change 버튼을 클릭하여 적 영웅들의 궁극기 게이지의 위치를 변경할 수 있습니다.

Change 버튼을 누른 후 게이지를 좌클릭한 채로 마우스로 끌어 원하는 위치로 옮긴 후 좌클릭을 떼면 자동으로 새 위치로 게이지가 옮겨집니다.

## Ultimate Charge Filter Percent

지정된 퍼센트보다 궁극기 충전률이 높은 적 영웅들의 궁극기 충전률 게이지만 나타냅니다.

지정된 퍼센트보다 궁극기 충전률이 낮은 적 영웅들의 궁극기 충전률은 목록에서 숨겨집니다.

## FoV Circle

에임봇의 FoV(시야각)을 화면 상에 원 형태로 표시합니다. 이 원의 색상은 토글 버튼 왼쪽의 색 팔레트를 클릭하여 변경할 수 있습니다.

이 원 안에 들어온 적들만 에임봇의 목표로 삼는다고 생각하면 됩니다.

<figure><img src="../../.gitbook/assets/showcase_fovcircle.png" alt=""><figcaption><p>사용 예시</p></figcaption></figure>

## FoV Radar

십자선을 중심으로 현재 주변 적들과 아군들의 위치를 나타내는 레이더를 표시합니다.

아군은 초록색으로 적은 빨간색으로 표시되며, 보이는 영웅은 선명한 점으로, 보이지 않는 영웅은 약간 투명한 점으로 표시됩니다.
이 때, 궁극기가 완전히 충전된 적은 빨간색 대신 주황색으로 표시됩니다.

멀리 있는 영웅일수록 점의 크기가 작게 표시됩니다. 이 때 수평 거리 상 더 멀리 있는 적보다 수직 거리 상 더 멀리 있는 적이 더 작게 표시됩니다.
즉, 점 크기에는 해당 영웅과의 수평 거리보다 수직 거리가 더 큰 영향을 미칩니다.

영웅이 처치당하거나, 리스폰 또는 부활할 경우 해당 영웅에 해당하는 점이 강조 표시됩니다.

<figure><img src="../../.gitbook/assets/showcase_fovradar.png" alt=""><figcaption><p>사용 예시</p></figcaption></figure>

## FoV Radar Size

FoV 레이더의 크기 배율을 조절합니다.

## FoV Radar Dot Size

FoV 레이더에 나타나는 점들의 크기 배율을 조절합니다.
