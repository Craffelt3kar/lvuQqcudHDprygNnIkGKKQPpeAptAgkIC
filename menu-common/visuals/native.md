---
description: 이 메뉴에서는 게임 내적으로 이미 존재하는 요소들을 이용하여 ESP를 구현하고 설정할 수 있습니다.
---

# Native: 네이티브 ESP

<figure><img src="../../.gitbook/assets/menu_visuals_native_spectator_en.png" alt=""><figcaption><p>NATIVE ESP</p></figcaption></figure>

## Outlines Type

* Spectator - 관전자 모드에서 보이는 것과 같이 영웅들에게 테두리를 씌워서 드러냅니다. 흔히 떠올리는 테두리 월핵(테월)과 동일합니다.

* Widowmaker - 위도우메이커의 적외선 투시경을 사용했을 때와 같이 영웅을 드러냅니다.

* Support - 힐러가 아군을 봤을 때 윤곽이 드러나듯이 영웅을 드러냅니다.

* None - ESP를 끕니다.

---

## Spectator

<figure><img src="../../.gitbook/assets/menu_visuals_native_spectator_en.png" alt=""><figcaption><p>Spectator 모드</p></figcaption></figure>

### Enable Enemies

적 영웅들에 대해 테두리 ESP를 나타낼지의 여부와, (만약 나타낸다면) 테두리의 색상을 지정합니다.

### Enable Allies

아군 영웅들에 대해 테두리 ESP를 나타낼지의 여부와, (만약 나타낸다면) 테두리의 색상을 지정합니다.

## Widowmaker, Support

<figure><img src="../../.gitbook/assets/menu_visuals_native_widowmaker_en.png" alt=""><figcaption><p>Widowmaker 모드</p></figcaption></figure>

### Enable Enemies

적 영웅들에 대해 ESP를 나타낼지의 여부를 지정합니다.

### Enable Allies

아군 영웅들에 대해 ESP를 나타낼지의 여부를 지정합니다.

### Disable Shields

방벽 및 보호막을 ESP 대상에서 제외시킬지의 여부를 지정합니다.

활성화 시 방벽 및 보호막은 ESP 대상에서 제외되어 드러나지 않습니다.

### Disable Objects

오브젝트(위도우메이커 덫, 토르비욘 포탑 등)을 ESP 대상에서 제외시킬지의 여부를 지정합니다.

활성화 시 오브젝트들은 ESP 대상에서 제외되어 드러나지 않습니다.

## 사용 사진

### Spectator

{% tabs %}
{% tab title="사진 1" %}
<figure><img src="../../.gitbook/assets/showcase_visuals_native_spectator.png" alt=""/><figcaption><p>사용 예시</p></figcaption></figure>
{% endtab %}

{% tab title="사진 2" %}
<figure><img src="../../.gitbook/assets/showcase_visuals_native_spectator2.png" alt=""/><figcaption><p>사용 예시</p></figcaption></figure>
{% endtab %}
{% endtabs %}

### Widowmaker

{% tabs %}
{% tab title="사진 1" %}
<figure><img src="../../.gitbook/assets/showcase_visuals_native_widowmaker.png" alt=""/><figcaption><p>사용 예시</p></figcaption></figure>
{% endtab %}

{% tab title="사진 2" %}
<figure><img src="../../.gitbook/assets/showcase_visuals_native_widowmaker2.png" alt=""/><figcaption><p>사용 예시</p></figcaption></figure>
{% endtab %}

{% tab title="사진 3" %}
<figure><img src="../../.gitbook/assets/showcase_visuals_native_widowmaker3.png" alt=""/><figcaption><p>사용 예시</p></figcaption></figure>
{% endtab %}
{% endtabs %}

### Support

{% tabs %}
{% tab title="사진 1" %}
<figure><img src="../../.gitbook/assets/showcase_visuals_native_support.png" alt=""/><figcaption><p>사용 예시</p></figcaption></figure>
{% endtab %}

{% tab title="사진 2" %}
<figure><img src="../../.gitbook/assets/showcase_visuals_native_support2.png" alt=""/><figcaption><p>사용 예시</p></figcaption></figure>
{% endtab %}

{% tab title="사진 3" %}
<figure><img src="../../.gitbook/assets/showcase_visuals_native_support3.png" alt=""/><figcaption><p>사용 예시</p></figcaption></figure>
{% endtab %}
{% endtabs %}
