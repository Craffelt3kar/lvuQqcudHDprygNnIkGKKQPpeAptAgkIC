---
description: 이 메뉴에서는 맵 상에 3D로 그려질 수 있는 ESP 요소들에 대해 설정할 수 있습니다.
---

# 3D Render: 3D ESP

<figure><img src="../../.gitbook/assets/menu_visuals_3drender_en.png" alt=""><figcaption><p>3D RENDER ESP</p></figcaption></figure>

## Critical Hitbox

적 영웅들의 치명타 판정 히트박스(헤드샷 히트박스)의 범위를 나타낼지 여부와, (만약 나타낸다면) 그 색상을 지정합니다.

<figure><img src="../../.gitbook/assets/showcase_crit_hitbox.png" alt=""><figcaption><p>사용 예시</p></figcaption></figure>

## Regular Hitbox

적 영웅들의 일반 히트박스(몸샷 히트박스)의 범위를 나타낼지 여부와, (만약 나타낸다면) 그 색상을 지정합니다.

<figure><img src="../../.gitbook/assets/showcase_body_hitbox.png" alt=""><figcaption><p>사용 예시</p></figcaption></figure>

## Disable Objects

적 오브젝트들의 히트박스 범위를 나타낼지의 여부와, (만약 나타낸다면) 그 색상을 지정합니다.

지원하는 오브젝트들은 다음과 같습니다:

* <img src="../../.gitbook/assets/hero_torbjorn.png" alt="" data-size="line"/>토르비욘 포탑<img src="../../.gitbook/assets/hero_torbjorn_shift.png" alt="" data-size="line"/>
* <img src="../../.gitbook/assets/hero_symmetra.png" alt="" data-size="line"/>시메트라 포탑<img src="../../.gitbook/assets/hero_symmetra_shift.png" alt="" data-size="line"/>
* <img src="../../.gitbook/assets/hero_illari.png" alt="" data-size="line"/>일리아리 태양석<img src="../../.gitbook/assets/hero_illari_e.png" alt="" data-size="line"/>

## Backtrack Hitbox

히트박스의 범위를 나타낼 때, 백트랙 옵션이 켜져 있다면(Special 탭의 `Backtrack MS`가 0보다 클 때) 이를 고려하여 히트박스를 나타냅니다.

{% tabs %}
{% tab title="사진 1" %}
<figure><img src="../../.gitbook/assets/showcase_backtrack_hitbox.png" alt=""/><figcaption><p>사용 예시</p></figcaption></figure>
{% endtab %}

{% tab title="사진 2" %}
<figure><img src="../../.gitbook/assets/showcase_backtrack_hitbox2.png" alt=""/><figcaption><p>사용 예시</p></figcaption></figure>
{% endtab %}

{% tab title="사진 3" %}
<figure><img src="../../.gitbook/assets/showcase_backtrack_hitbox3.png" alt=""/><figcaption><p>사용 예시</p></figcaption></figure>
{% endtab %}

{% tab title="사진 4" %}
<figure><img src="../../.gitbook/assets/showcase_backtrack_hitbox4.png" alt=""/><figcaption><p>사용 예시</p></figcaption></figure>
{% endtab %}
{% endtabs %}
