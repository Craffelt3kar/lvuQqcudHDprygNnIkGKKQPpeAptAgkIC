---
description: 설정을 변경할 영웅을 선택합니다.
---

# Heroes: 설정 대상 영웅 선택

<figure><img src="../../.gitbook/assets/menu_settings_heroes_en.png" alt=""><figcaption><p>SELECT HERO TO CHANGE SETTINGS</p></figcaption></figure>

말 그대로 설정을 변경할 영웅을 선택합니다. 영웅의 아이콘을 클릭함으로서 선택할 수 있습니다.

이 메뉴를 통해 현재 플레이 중인 영웅이 아닌 다른 영웅들에 대한 설정을 변경하는 등의 활용이 가능합니다.

이 메뉴는 와이번 메뉴 좌상단에 위치한 영웅 아이콘을 클릭해도 열 수 있습니다.
