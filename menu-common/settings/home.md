---
description: 와이번을 선택해 주셔서 감사합니다.
---

# Home: 와이번 홈

<figure><img src="../../.gitbook/assets/menu_settings_home_en.png" alt=""><figcaption><p>WELCOME TO WYVERN</p></figcaption></figure>

와이번에 대한 짧은 소개글과 함께 공식 디스코드 서버에 접속할 수 있는 링크와 QR코드를 제공합니다.

아래쪽에는 현재 남은 와이번 사용 가능 시간이 표시됩니다.

QR코드 아래에 있는 키를 클릭하여 새로운 키를 지정함으로서 메뉴 키를 바꿀 수 있습니다.
