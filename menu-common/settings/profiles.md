---
description: 설정 프로필 관리 메뉴입니다. 다양한 프로필을 생성, 제거, 가져오거나 내보내고 삭제할 수 있습니다.
---

# Profiles: 프로필 관리

<figure><img src="../../.gitbook/assets/menu_settings_profiles_en.png" alt=""><figcaption><p>PROFILES</p></figcaption></figure>

중앙의 목록에는 현재 사용 가능한 설정 프로필들이 표시됩니다.

사용을 원하시는 프로필을 클릭하면 해당 프로필의 설정이 자동으로 적용됩니다.

현재 사용 중인 프로필은 약간 더 밝게 표시되어 있습니다.

## New Profile

새로운 설정 프로필을 생성합니다.

클릭 시 아래와 같은 Create New Profile 창이 나타납니다.

<figure><img src="../../.gitbook/assets/menu_settings_profiles_newprofile_en.png" alt=""><figcaption><p>Create New Profile</p></figcaption></figure>

값들의 입력이 완료된 후에는 `Create` 버튼을 클릭해 프로필을 생성할 수 있으며, `Cancel` 버튼을 클릭하여 프로필 생성을 취소할 수 있습니다.

### Enter Profile Name

새로 생성할 프로필의 이름을 지정합니다.

### Enter Profile Description

새로 생성할 프로필의 간단한 설명을 지정합니다.

### Clone Existing Profile

새로운 프로필을 만드는 대신, 다른 프로필의 설정을 복제해 옵니다.

활성화 시 아래에 Select Profile To Clone Settings From 란이 표시됩니다.

<figure><img src="../../.gitbook/assets/menu_settings_profiles_newprofile_clone2_en.png" alt=""><figcaption><p>Create New Profile</p></figcaption></figure>

### Select Profile To Clone Settings From

설정을 가져올 다른 프로필을 지정합니다.

프로필의 이름을 클릭하여 지정할 수 있으며, 현재 선택된 프로필은 약간 더 밝게 표시됩니다.

## Export

현재 선택한 프로필을 내보냅니다. 클릭 시 새로운 설정 코드가 생성되어 클립보드로 복사됩니다.

설정 적용을 위해서는 Import란에 이 공유된 코드를 붙여넣기해 주면 됩니다.

현재 사용 중인 설정을 다른 사람에게 공유하고 싶다면 이 설정 코드를 공유하면 됩니다.

## Import

공유된 설정 코드에서 프로필을 불러옵니다.

<figure><img src="../../.gitbook/assets/menu_settings_profiles_import_en.png" alt=""><figcaption><p>Import Profile</p></figcaption></figure>

### Enter The Share-Code

공유된 설정 코드를 입력하는 입력란입니다.

Export 기능을 통해 공유된 설정 코드를 붙여넣으면 해당 코드로 공유된 설정 프로필이 목록에 나타납니다.

## Delete

현재 선택된 프로필을 삭제합니다.
