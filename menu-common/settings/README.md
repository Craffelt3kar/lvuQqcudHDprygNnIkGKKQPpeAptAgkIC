# Settings: 설정

{% content-ref url="profiles.md" %}
[Profiles: 프로필 관리](profiles.md)
{% endcontent-ref %}

{% content-ref url="heroes.md" %}
[Heroes: 설정 대상 영웅 선택](heroes.md)
{% endcontent-ref %}

{% content-ref url="home.md" %}
[Home: 와이번 홈](home.md)
{% endcontent-ref %}
