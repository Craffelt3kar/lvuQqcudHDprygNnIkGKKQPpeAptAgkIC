---
description: 와이번은 다양한 기능과 뛰어난 보안, 부드러운 에임을 자랑하는 인터널 치트 제품입니다.
---

# 와이번 소개

{% hint style="info" %}
한국 공식 서버는 [여기로!](https://discord.gg/Qr2FdWFaSd)

구매 문의는 도도(hayang._.) 님께 DM!
{% endhint %}

<img src=".gitbook/assets/wyvern.webp" alt=""/>

와이번은 모든 Intel, AMD CPU를 지원하며, 모든 Windows 10, 11 버전을 지원합니다.

배틀넷, 스팀 버전의 오버워치를 모두 지원하며, 전체화면 및 테두리 없는 창 모드에서 모두 사용 가능합니다.

런처 프로그램 내에 HWID 스푸퍼가 내장되어 있기에 하드밴 없이 안전하게 사용하실 수 있습니다.

## 지원 기능

* Windows 10, 11의 모든 버전을 지원합니다.
* 모든 Intel, AMD CPU를 지원합니다.
* 전체 화면 모드를 지원합니다.
* 피시방에서 사용 가능합니다.
* 런처, 인게임 메뉴 모두 한국어를 지원합니다.
* 프레임 드랍이 전혀 없습니다.
* 업데이트가 존재하면 자동으로 패치됩니다. (매 업데이트마다 파일을 다시 다운로드받으실 필요가 없습니다)
* 프로그램 내부에 스푸퍼가 내장되어 있습니다.

* ESP
  * 아웃라인 ESP, 위도우메이커 ESP, 힐러 ESP와 같이 다양한 ESP 모드르 지원합니다.
  * 아군 영웅 및 적 영웅 둘 다 지원합니다.
  * 방벽 및 오브젝트(토르비욘 포탑, 시메트라 포탑 등) 표시가 가능합니다.
  * 헤드샷, 몸샷 히트박스 ESP 표시가 가능합니다.
  * 적 영웅들의 궁극기 게이지 ESP 표시가 가능합니다.
  * FoV 범위 표시가 가능합니다.
  * 2D 레이더 기능을 지원합니다.
  
* 에임봇
  * 트래킹, 플릭, 에임어시스트(고스트에임), 트리거봇(오토샷), 기술 에임봇 다섯 종류의 에임봇을 지원합니다.
  * 투사체 예측, 탄 낙차 예측 지원합니다 (한조, 토르비욘 등)
  * 백트랙(히스토리) 및 사일런트 에임(레이지 모드)를 지원합니다.
  * 탄퍼짐 제거 기능을 지원합니다.
  * 무반동(반동억제) 기능을 지원합니다.
  * 다양한 스킬의 사용을 보조해 줍니다. (라인하르트 화염 강타, 로드호그 그랩, 아나 수면총, ...)

## 설명서 제작자

by RS-232C (._.headl3ss)

자유롭게 퍼가고, 사용하고, 수정하셔도 됩니다.
단, 라이센스인 CC-BY-SA 4.0에 규정된 내용을 준수해 주세요.
수정 후 재배포 시 DM 한번씩 부탁드립니다.

설명서 소스:

{% embed url="https://gitlab.com/Craffelt3kar/lvuQqcudHDprygNnIkGKKQPpeAptAgkIC" %}

설명서에서 잘못된 정보나 오류, 오타 등을 발견 시 디스코드로 DM을 주시거나, 위 소스 코드 레포지터리에서 Issue 또는 Merge Request를 제출해 주세요.

## 설명서 라이센스

<img src=".gitbook/assets/200_by-sa.png" alt=""/>

이 설명서는 CC-BY-SA 4.0 라이센스로 배포됩니다.

{% embed url="https://creativecommons.org/licenses/by-sa/4.0/deed.ko" %}

---

와이번 설명서 by RS-232C (._.headl3ss)

와이번 설명서 is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.
