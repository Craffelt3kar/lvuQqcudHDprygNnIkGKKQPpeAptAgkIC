---
description: 에임에 관련된 특수 기능들을 제공합니다.
---

# Special: 특수 기능

<figure><img src="../.gitbook/assets/menu_aimbot_special_en.png" alt=""><figcaption><p>SPECIAL SETTINGS</p></figcaption></figure>

## Rage Mode

레이지 모드(사일런트 에임)을 활성화할지의 여부입니다.

활성화 시 말 그대로 총알이 '유도탄처럼' 자동으로 목표를 찾아 날라갑니다.

{% hint style="danger" %}
**사용 시 빠른 시간 안에 확정적으로 밴을 먹습니다.**

반드시 버릴 계정에서만 사용해 주세요.
{% endhint %}

## Compensate Aim Punch / Recoil

반동 억제(무반동) 기능을 활성화할지의 여부입니다.

활성화 시 위도우메이커, 캐서디, 솔저: 76과 같은 기본 무기가 반동을 가지는 영웅들의 무기 반동을 완전히 제거합니다.

{% hint style="danger" %}
**안전하지 않은 기능입니다.**

사용 시 밴을 먹을 확률이 높으니 버릴 계정에서만 사용하시는 것을 추천합니다.
{% endhint %}

## Backtrack MS

백트랙 기능을 활성화할지의 여부와 강도를 조절합니다.

백트랙 기능은 히스토리, 욘두핵이라고도 [랙 컴펜세이션(Lag Compensation)](https://developer.valvesoftware.com/wiki/Lag_Compensation) 기능을 악용하여 핑을 건드려 히트박스의 범위를 강제로 키우는 기법입니다.

자세한 설명과 원리는 다음 영상을 참고하세요(영문):

{% embed url="https://youtu.be/JXzkcKP_qFU" %}

0보다 큰 값을 지정 시 백트랙 기능이 활성화되며, 최대 몇 밀리초 전의 목표까지 백트랙 기능으로 끌어올지 지정할 수 있습니다.

{% hint style="danger" %}
**안전하지 않은 기능입니다.**

2023년 9월 말 경부터 블리자드 측에서 플레이어의 핑을 감시하는 시스템을 도입하였습니다.

이 때문에 높은 백트랙 값을 사용하거나, 낮은 값을 사용하더라도 너무 장기간 동안 백트랙을 사용하면 반드시 밴을 먹도록 바뀌었습니다.

또한 높은 값을 사용하면 킬캠이나 POTG, 리플레이 등에서 대놓고 티가 납니다. (한조 화살이 날아가다 갑자기 꺾인다던지...)

오래 사용하실 계정에서는 아예 사용하지 않거나, 사용하더라도 낮은 값으로 잠깐잠깐씩만 사용하시는 것을 추천드립니다.
{% endhint %}
