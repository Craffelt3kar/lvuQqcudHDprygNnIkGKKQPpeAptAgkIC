# Aimbot: 에임봇

<figure><img src="../.gitbook/assets/menu_aimbot_flick_en.png" alt=""><figcaption><p>에임봇 예시</p></figcaption></figure>

## Target Selector Settings

### Aimbot

### Field of View (시야)

에임봇의 시야각 제한값입니다.

십자선을 기준으로 지정된 반지름을 가진 원을 그린 후, 해당 원 안에 들어온 목표들만 조준 대상으로 삼는다고 생각하면 됩니다.

직관적으로 시야를 조절할 수 있도록 돕기 위해, 슬라이더에 마우스 커서를 올리면 자동으로 에임봇의 시야를 표시하는 원이 화면에 드러납니다.

### Max Distance

목표로 삼을 영웅과의 최대 거리입니다. (단위: 미터)

이 거리보다 멀리 있는 적 영웅들은 조준 대상으로 삼지 않습니다.

### Smart Target Selection

활성화 시 목표 선택을 비교적으로 덜 공격적으로 수행합니다.

### Aim Mode

* Default - 가장 가까운 히트박스를 조준 대상으로 삼습니다.

* Prefer Head - 머리 히트박스(치명타 히트박스)를 우선하여 조준합니다. 만약 머리가 보이지 않는다면 가장 가까운 히트박스를 조준 대상으로 삼습니다.

## Additional Hero Settings

{% hint style="information" %}
이 항목은 영웅별로 존재할 수도 있고, 존재하지 않을 수도 있습니다.
{% endhint %}

몇몇 특정 영웅들에게만 적용되는 에임봇 옵션들이 추가되는 그룹입니다.

### Minimum (...) Charge

무기가 지정된 퍼센트 이상 충전되었을 때만 에임키를 눌렀을 때 에임봇이 작동합니다.

### Shoot On Release

활성화 시 에임키를 '눌렀을 때' 발사하는 것이 아닌, '뗐을 때' 에임봇이 동작하여 목표를 조준하고 발사합니다.

## Current Aimbot Settings

* Flick Aimbot - 플릭 에임봇 설정 탭을 보여줍니다.

* Track Aimbot - 트래킹 에임봇 설정 탭을 보여줍니다.

* Aim Assist - 에임 어시스트 설정 탭을 보여줍니다.

* Trigger Bot - 트리거봇 설정 탭을 보여줍니다.

* Ability Aimbot - 기술 에임봇 설정 탭을 보여줍니다.

자세한 내용은 각 설정 탭에 해당하는 문서를 참고하세요.

{% content-ref url="flick.md" %}
[Flick Aimbot: 플릭 에임봇](flick.md)
{% endcontent-ref %}

{% content-ref url="track.md" %}
[Track Aimbot: 트래킹 에임봇](track.md)
{% endcontent-ref %}

{% content-ref url="assist.md" %}
[Aim Assist: 에임 어시스트](assist.md)
{% endcontent-ref %}

{% content-ref url="triggerbot.md" %}
[Trigger Bot: 트리거봇](triggerbot.md)
{% endcontent-ref %}

{% content-ref url="ability.md" %}
[Ability Aimbot: 기술 에임봇](ability.md)
{% endcontent-ref %}

---

에임봇 탭의 하위 탭 중 제일 상단에 위치한 `Special` 탭에 대해서는 다음 문서를 참고하세요:

{% content-ref url="special.md" %}
[Special: 특수 기능](special.md)
{% endcontent-ref %}
