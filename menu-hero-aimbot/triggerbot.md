---
description: Trigger Bot - 트리거봇의 설정에 관해 설명합니다.
---

# Trigger Bot: 트리거봇

트리거봇은 '오토샷'의 다른 이름입니다. 말 그대로 목표가 십자선에 닿으면 자동으로 총을 발사해 주는 기능을 합니다.

<figure><img src="../.gitbook/assets/menu_aimbot_triggerbot_adv_en.png" alt=""><figcaption><p>Trigger Bot Settings</p></figcaption></figure>

## Enabled

트리거봇을 활성화할지 여부를 지정합니다.

## Shoot Shields

트리거봇이 방벽 또는 보호박을 쏠 것인지 여부를 지정합니다.

## Aim Key

트리거봇을 활성화시킬 에임키입니다.

키 버튼을 클릭하면 '...'으로 바뀌는데, 이 때 원하는 키를 눌러 키를 지정할 수 있습니다.
또한, ESC키를 눌러 키 입력 모드에서 벗어날 수 있습니다.

{% hint style="information" %}
에임키를 좌클릭 등으로 설정하더라도, 일단 총이 발사되고 나서 에임봇이 작동하는 등 문제의 문제가 발생하지 않고 정상적으로 목표를 조준한 후 샷을 발사합니다.
{% endhint %}

## Block Input Without Target

기본적으로 FoV 내에 목표가 존재하지 않는 상태에서 에임키를 누를 시, 트리거봇은 그 자리에서 무기를 발사합니다.

이를 활성화 시, FoV 내에 목표가 존재하지 않는 경우에는 에임키를 누르더라도, 심지어 좌클릭을 누르더라도 무기를 발사하지 않습니다.

## Block Input On Obscured Targets

활성화 시 보이지 않는 목표에 대해서는 에임키를 누르더라도, 심지어 좌클릭을 누르더라도 절대로 총을 발사하지 않습니다.

## Time Until Shootable (On Screen)

화면 안에 들어온 목표를 포착하여 쏠 수 있게 되기까지 대기할 시간입니다.

이 값이 존재하는 의도는 바로 화면 상에 아주 잠깐동안만 비춰졌던 목표를 초인적인 반응속도로 포착하여 쏴 버리는 일을 막기 위함입니다.

## Time Until Shootable (Visible)

안 보이다가 보이게 된 목표를 쏠 수 있게 되기까지 기다릴 시간입니다.

이 값이 존재하는 의도는 바로 갑자기 모습을 드러낸 목표를 초인적인 반응속도로 포착하여 쏴 버리는 일을 막기 위함입니다.
