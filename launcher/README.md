# 와이번 런처

와이번 및 스푸퍼를 사용하기 위해서는 먼저 와이번 런처를 다운로드받으셔야 합니다.

## [와이번 런처 다운로드 링크](https://wyvern.cx/download)

다운로드 후 파일을 관리자 권한으로 실행합니다. 만약 안티바이라스의 실시간 감시 기능이 켜져 있다면 비활성화하셔야 합니다.

## 로그인 창

와이번 로더를 처음 실행하면 먼저 로그인 창이 뜰 것입니다.

이미 계정을 가지고 계시다면, 이 때 유저이름과 비밀번호를 입력하여 로그인하면 됩니다.

<figure><img src="../.gitbook/assets/launcher_login_ko.png" alt=""><figcaption><p>로그인</p></figcaption></figure>

계정을 가지고 계시지 않다면, 윗쪽의 `지금 등록하세여!` 버튼을 클릭하여 계정 생성 창으로 넘어갈 수 있습니다.

## 등록 (계정 생성) 창

계정을 가지고 계시지 않다면 이 창에서 생성할 수 있습니다.

<figure><img src="../.gitbook/assets/launcher_register_ko.png" alt=""><figcaption><p>등록</p></figcaption></figure>

사용하고자 하는 유저이름과 비밀번호를 입력하고, `Register` 버튼을 클릭하면 됩니다.

이미 계정을 가지고 계시다면 `로그인 Now` 버튼을 클릭하여 로그인 창으로 넘어갈 수 있습니다.

## 대시보드

와이번의 메인 화면이자 대시보드입니다.

<figure><img src="../.gitbook/assets/launcher_dashboard_ko.png" alt=""><figcaption><p>대시보드</p></figcaption></figure>

정면에는 지원하는 제품들의 목록이 나타납니다.

우측 상단에는 현재 남은 크레딧과 크레딧 충전 버튼 (노란색 `+` 모양 버튼)이 표시됩니다.

하단에는 현재 하이퍼바이저, 드라이버, 네트워크 및 인젝터의 상태가 나타납니다.

### 스푸퍼

커널 모드 스푸퍼 드라이버 기능에 대한 대시보드입니다.

<figure><img src="../.gitbook/assets/launcher_spoofer_ko.png" alt=""><figcaption><p>스푸퍼</p></figcaption></figure>

무작위 HWID 시드를 생성 또는 적용할 수 있고, 컴퓨터에 남아있는 HWID 추적 데이터들을 삭제할 수 있습니다.

이 때, 무작위 HWID 시드를 생성하고 적용하기 위해선는 스푸퍼 드라이버가 로드되어 있어야 합니다.

`LOAD SPOOFER` 버튼을 눌러 커널 모드 스푸퍼 드라이버를 로드할 수 있습니다.

### 오버워치

와이번 오버워치 구독권을 구매하여 사용할 수 있습니다.

<figure><img src="../.gitbook/assets/launcher_overwatch_ko.png" alt=""><figcaption><p>오버워치 (2시간 테스트 코드 사용 가능)</p></figcaption></figure>

<figure><img src="../.gitbook/assets/launcher_overwatch_active_ko.png" alt=""><figcaption><p>오버워치 (구독 구매 완료)</p></figcaption></figure>

구매 즉시 사용권의 시간이 흐르기 시작합니다. 가능한 한 자주, 그리고 빨리 사용해 주시기 바랍니다.

<figure><img src="../.gitbook/assets/launcher_overwatch_loaded_ko.png" alt=""><figcaption><p>오버워치 (와이번 로드 완료)</p></figcaption></figure>

또한, 한번 드라이버가 로드되면 오버워치를 종료하더라도 오버워치를 다시 켤 때 자동으로 와이번이 다시 인젝트됩니다. 즉, 게임이 꺼져서 다시 인젝트해야 할 때 다시 와이번 런처를 실행시켜 인젝터를 실행시켜 주실 필요가 없습니다!

와이번을 언로드하여 다음 번에 오버워치를 시작할 때는 와이번을 인젝트하지 않으려면, 런처를 열어 구독권 구매란 아래 존재하는 'UNLOAD' 버튼을 클릭해 와이번을 언로스시켜 주시기 바랍니다.

다른 게임을 플레이하기 위해 와이번 드라이버를 완전히 언로드하시려면 컴퓨터를 재부팅해 주시기 바랍니다. (되도록이면 '다시 시작' 기능을 이용해 재부팅해 주시고, 빠른 시작 모드는 비활성화해 주셔야 합니다.)

## 크레딧 충전

이 메뉴에서는 와이번 크레딧을 충전하거나, 크레딧 구매처로 이동할 수 있습니다.

<figure><img src="../.gitbook/assets/launcher_redeem_ko.png" alt=""><figcaption><p>크레딧 충전 메뉴</p></figcaption></figure>

좌측의 `Redeem` 아래 입력란에 키를 입력한 후 아래 `Redeem Key(s)` 버튼을 눌러 크레딧을 충전할 수 있습니다.

한 번에 두 개 이상의 키도 한꺼번에 충전이 가능합니다.

키 입력란과 `Redeem Key(s)` 버튼 사이에 있는 아이콘들을 클릭하여 해당 지불 방법을 지원하는 판매자 사이트 또는 서버로 접속할 수 있습니다.

우측에는 크레딧 충전 및 사용 기록이 나타납니다.

## 설정

계정 설정(비밀번호 변경) 기능을 제공하는 메뉴입니다.

<figure><img src="../.gitbook/assets/launcher_settings_ko.png" alt=""><figcaption><p>설정</p></figcaption></figure>

왼쪽의 계정 비밀번호 변경 메뉴에서는 현재 사용 중인 비밀번호와 새로 사용할 비밀번호를 입력함으로서 현재 계정의 비밀번호를 변경할 수 있습니다.
