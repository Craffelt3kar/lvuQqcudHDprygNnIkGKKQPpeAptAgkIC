# Table of contents

## 와이번 소개 <a href="#introduction" id="introduction"></a>

* [와이번 소개](README.md)

## 와이번 런처 <a href="#launcher" id="launcher"></a>

* [와이번 런처](launcher/README.md)
* [사용 전 체크리스트](launcher/checklist.md)
* [크레딧과 구독권](launcher/credit-and-sub.md)
* [와이번 런처 다운로드 링크](https://wyvern.cx/download)

## 설정 메뉴 <a href="#menu" id="menu"></a>

* [와이번 메뉴](menu/README.md)

## 영웅 별 설정 탭 (Hero) <a href="#menu-hero" id="menu-hero"></a>

* [Aimbot: 에임봇](menu-hero-aimbot/README.md)
  * [Special: 특수 기능](menu-hero-aimbot/special.md)
  * [Flick Aimbot: 플릭 에임봇](menu-hero-aimbot/flick.md)
  * [Track Aimbot: 트래킹 에임봇](menu-hero-aimbot/track.md)
  * [Aim Assist: 에임 어시스트](menu-hero-aimbot/assist.md)
  * [Trigger Bot: 트리거봇](menu-hero-aimbot/triggerbot.md)
  * [Ability Aimbot: 기술 에임봇](menu-hero-aimbot/ability.md)

## 전역 설정 탭 (Common) <a href="#menu-common" id="menu-common"></a>

* [Visuals: 시각 기능](menu-common/visuals/README.md)
  * [Native: 네이티브 ESP](menu-common/visuals/native.md)
  * [2D Render: 2D ESP](menu-common/visuals/2drender.md)
  * [3D Render: 3D ESP](menu-common/visuals/3drender.md)
* [Misc: 기타 기능](menu-common/misc/README.md)
  * [Auto Melee: 자동 근접 공격](menu-common/misc/automelee.md)
  * [Auto Lock Hero: 자동 영웅 고정](menu-common/misc/autolockhero.md)
  * [Exploits: 취약점 악용](menu-common/misc/exploits.md)
* [Settings: 설정](menu-common/settings/README.md)
  * [Profiles: 프로필 관리](menu-common/settings/profiles.md)
  * [Heroes: 설정 대상 영웅 선택](menu-common/settings/heroes.md)
  * [Home: 와이번 홈](menu-common/settings/home.md)

## 링크 <a href="#links" id="links"></a>

* [와이번 공식 한국 디스코드 서버](https://discord.gg/Qr2FdWFaSd)
* [와이번 공식 한국 디스코드 서버 #와이번 채널](https://discord.com/channels/950288658085322752/1147572137125757059)
* [와이번 공식 영문 디스코드 서버](https://wyvern.cx/discord)
