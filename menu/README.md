# 와이번 메뉴

## 메뉴 키

메뉴를 여는 키는 기본적으로 `Home`키입니다.

<figure><img src="../.gitbook/assets/menukey.png" alt=""><figcaption><p>Drawn by Mysid in CorelDRAW.</p></figcaption></figure>

메뉴 키는 [Home: 와이번 홈](../menu-common/settings/home.md) 탭에서 변경할 수 있습니다.

{% content-ref url="../menu-common/settings/home.md" %}
[Home: 와이번 홈](../menu-common/settings/home.md)
{% endcontent-ref %}

## 메뉴 구성

<figure><img src="../.gitbook/assets/menu_settings_home_en.png" alt=""><figcaption><p>와이번 홈 메뉴</p></figcaption></figure>

좌측 상단에는 현재 플레이 중인 영웅의 아이콘이 나타납니다. 영웅 아이콘을 클릭하면 [영웅 선택 메뉴](../menu-common/settings/heroes.md)로 넘어갑니다.

그 아래에는 각 설정 탭들이 나열되어 있습니다. 이에 대한 자세한 설명은 [설정 탭](#설정-탭) 문단을 참고하세요.

맨 아래, 즉 좌측 하단에는 국기 아이콘이 있으며, 이를 클릭하면 메뉴의 언어를 변경 가능합니다.

## 설정 탭

설정 탭은 탭들이 묶여 있는 그룹을 기준으로 크게 `Hero` 카테고리와 `Common` 카테고리로 분류됩니다.

`Hero` 카테고리에는 현재 `Aimbot` 탭밖에 존재하지 않으며, 이 탭에 대한 자세한 설명은 [Aimbot: 에임봇](../menu-hero-aimbot/README.md) 문서를 참고하세요.

{% content-ref url="../menu-hero-aimbot/README.md" %}
[Aimbot: 에임봇](../menu-hero-aimbot/README.md)
{% endcontent-ref %}

---

`Common` 카테고리에는 현재 `Visuals`, `Misc`, `Settings` 세 개의 탭이 속해 있으며, 각 탭들에 대한 설명들은 각각 다음 문서들을 확인하세요.

{% content-ref url="../menu-common/visuals/README.md" %}
[Visuals: 시각 기능](../menu-common/visuals/README.md)
{% endcontent-ref %}

{% content-ref url="../menu-common/misc/README.md" %}
[Misc: 기타 기능](../menu-common/misc/README.md)
{% endcontent-ref %}

{% content-ref url="../menu-common/settings/README.md" %}
[Settings: 설정](../menu-common/settings/README.md)
{% endcontent-ref %}
